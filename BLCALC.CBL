       IDENTIFICATION DIVISION.
       PROGRAM-ID. BLCALC.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.

       DATA DIVISION.
       FILE SECTION.

       WORKING-STORAGE SECTION.
       01  WS-VARIABLES.
           05  WS-VALUE-1      PIC S9(4).
           05  WS-VALUE-2      PIC S9(4).
           05  COMMAND         PIC 9        VALUE 0.
           05  RESULT          PIC S9(4)    VALUE 0.
           05  RESULT-MESSAGE  PIC X(30)    VALUE SPACES.


       01  COMMAND-DEFINES.
           05  CMD-UNKNWOWN    PIC S9 VALUE -1.
           05  CMD-ADD         PIC S9 VALUE 1.
           05  CMD-SUBSTRACT   PIC S9 VALUE 2.
           05  CMD-MULTIPLY    PIC S9 VALUE 3.
           05  CMD-DIVIDE      PIC S9 VALUE 4.


       LINKAGE SECTION.
       01  LS-VALUE-1       PIC S9.
       01  LS-VALUE-2       PIC S9.
       01  LS-COMMAND       PIC X(1) VALUE SPACE.


       PROCEDURE DIVISION USING LS-VALUE-1, LS-VALUE-2, LS-COMMAND.
           PERFORM INITIALISE-VARIABLES
           PERFORM MAIN
           PERFORM GET-GENERATED-MESSAGE
           GOBACK
           .

       MAIN.
          EVALUATE LS-COMMAND
               WHEN "+"
                   PERFORM ADD-VALUES
               WHEN "-"
                   PERFORM SUBSTRACT-VALUES
               WHEN "*"
                   PERFORM MULTIPLY-VALUES
               WHEN "/"
                   PERFORM DIVIDE-VALUES
               WHEN OTHER
                   MOVE CMD-UNKNWOWN TO COMMAND
           END-EVALUATE
           .

       INITIALISE-VARIABLES.
           MOVE LS-VALUE-1 TO WS-VALUE-1
           MOVE LS-VALUE-2 TO WS-VALUE-2
           .

       ADD-VALUES.
           MOVE CMD-ADD TO COMMAND
           COMPUTE RESULT = WS-VALUE-1 + WS-VALUE-2
           .

       SUBSTRACT-VALUES.
           MOVE CMD-SUBSTRACT TO COMMAND
           COMPUTE RESULT = WS-VALUE-1 - WS-VALUE-2
           .

       MULTIPLY-VALUES.
           MOVE CMD-MULTIPLY TO COMMAND
           COMPUTE RESULT = WS-VALUE-1 * WS-VALUE-2
           .

       DIVIDE-VALUES.
           MOVE CMD-DIVIDE TO COMMAND
           COMPUTE RESULT = WS-VALUE-1 / WS-VALUE-2
           .

       GET-GENERATED-MESSAGE.
           CALL 'GENRESMSG' USING LS-VALUE-1, LS-VALUE-2, RESULT,
                                  COMMAND, RESULT-MESSAGE
           END-CALL
           .
